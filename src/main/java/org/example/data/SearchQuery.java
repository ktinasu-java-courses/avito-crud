package org.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SearchQuery {
    public static final int MAX_ROOMS_NUMBER_SEARCH = 5;
    private boolean studio;
    private boolean freeLayout;
    private int roomsNumberMin;
    private int roomsNumberMax;
    private int priceMin;
    private int priceMax;
    private double areaMin;
    private double areaMax;
    private boolean hasBalcony;
    private boolean hasLoggia;
    private int floorNumberMin;
    private int floorNumberMax;
    private boolean notFirst;
    private boolean notLast;
    private int floorNumberInHouseMin;
    private int floorNumberInHouseMax;
}
