package org.example.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Apartment {
    private long id;
    private int roomsNumber;
    private boolean studio;
    private boolean freeLayout;
    private int price;
    private double area;
    private boolean hasBalcony;
    private boolean hasLoggia;
    private int floorNumber;
    private int floorNumberInHouse;
    private String address;
    private String description;

    public Apartment(int roomsNumber, boolean studio, boolean freeLayout, int price, double area,
                     boolean hasBalcony, boolean hasLoggia, int floorNumber, int floorNumberInHouse, String address, String description) {
        this(0, roomsNumber, studio, freeLayout, price, area, hasBalcony, hasLoggia, floorNumber, floorNumberInHouse, address, description);
    }
}
