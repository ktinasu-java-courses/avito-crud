package org.example.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.data.Apartment;
import org.example.data.SearchQuery;
import org.example.exception.ApartmentNotFoundException;

import java.util.ArrayList;
import java.util.List;


@Slf4j
public class AvitoManager {
    private static final int SEARCH_SIZE_LIMIT = 5;
    private static final int MAX_ROOMS_IN_APARTMENT = 99;
    private final List<Apartment> items = new ArrayList<>(100);
    private long nextId = 1;

    public List<Apartment> getAll() {
        log.debug("get all apartments, quantity: {}", items.size());
        return new ArrayList<>(items);
    }

    public Apartment getById(final long id) {
        for (final Apartment item : items) {
            if (item.getId() == id) {
                log.debug("get apartment by id: {}", id);
                return item;
            }
        }
        throw new ApartmentNotFoundException("apartment with id " + id + " not found");
    }

    public Apartment create(final Apartment item) {
        log.debug("create apartment, nextId: {}, address: {}", nextId, item.getAddress());
        item.setId(nextId);
        nextId++;
        items.add(item);
        return item;
    }

    public Apartment update(final Apartment item) {
        log.debug("update apartment, id: {}", item.getId());
        final int index = getIndexById(item.getId());
        if (index == -1) {
            throw new ApartmentNotFoundException("impossible to update apartment with id " + item.getId() + " (not found)");
        }
        items.set(index, item);
        log.debug("apartment updated, id: {}", item.getId());
        return item;
    }

    public boolean removeById(final long id) {
        log.debug("removing by id: {}", id);
        return items.removeIf(o -> o.getId() == id);
    }

    public int getCount() {
        return items.size();
    }

    public List<Apartment> search(final SearchQuery search) {
        final List<Apartment> results = new ArrayList<>(SEARCH_SIZE_LIMIT);
        log.debug("search by query: {}", search);
        for (final Apartment item : items) {
            if (matches(item, search)) {
                results.add(item);
                if (results.size() >= SEARCH_SIZE_LIMIT) {
                    return results;
                }
            }
        }
        return results;
    }

    private int getIndexById(final long id) {
        for (int i = 0; i < items.size(); i++) {
            final Apartment product = items.get(i);
            if (product.getId() == id) {
                return i;
            }
        }
        return -1;
    }

    private boolean matches(final Apartment apartment, final SearchQuery query) {
        if (apartment.isFreeLayout() != query.isFreeLayout()) {
            return false;
        }
        if (apartment.isHasBalcony() != query.isHasBalcony()) {
            return false;
        }
        if (apartment.isHasLoggia() != query.isHasLoggia()) {
            return false;
        }
        if (apartment.isStudio() != query.isStudio()) {
            return false;
        }
        if (apartment.getArea() < query.getAreaMin()) {
            return false;
        }
        if (apartment.getArea() > query.getAreaMax()) {
            return false;
        }
        if (apartment.getFloorNumber() < query.getFloorNumberMin()) {
            return false;
        }
        if (apartment.getFloorNumber() > query.getFloorNumberMax()) {
            return false;
        }
        if (apartment.getFloorNumberInHouse() < query.getFloorNumberInHouseMin()) {
            return false;
        }
        if (apartment.getFloorNumberInHouse() > query.getFloorNumberInHouseMax()) {
            return false;
        }
        if (apartment.getFloorNumber() == 1 && query.isNotFirst()) {
            return false;
        }
        if (apartment.getFloorNumber() == apartment.getFloorNumberInHouse() && query.isNotLast()) {
            return false;
        }
        if (apartment.getPrice() < query.getPriceMin()) {
            return false;
        }
        if (apartment.getPrice() > query.getPriceMax()) {
            return false;
        }
        if (query.getRoomsNumberMax() >= query.MAX_ROOMS_NUMBER_SEARCH) {
            query.setRoomsNumberMax(MAX_ROOMS_IN_APARTMENT);
        }
        if (apartment.getRoomsNumber() < query.getRoomsNumberMin()) {
            return false;
        }
        if (apartment.getRoomsNumber() > query.getRoomsNumberMax()) {
            return false;
        }
        return true;
    }
}
