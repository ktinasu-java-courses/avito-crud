package org.example.exception;

public class ApartmentNotFoundException extends RuntimeException {
    public ApartmentNotFoundException() {
    }

    public ApartmentNotFoundException(String message) {
        super(message);
    }

    public ApartmentNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApartmentNotFoundException(Throwable cause) {
        super(cause);
    }

    public ApartmentNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
