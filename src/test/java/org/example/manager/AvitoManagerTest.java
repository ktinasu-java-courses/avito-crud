package org.example.manager;

import org.example.data.Apartment;
import org.example.data.SearchQuery;
import org.example.exception.ApartmentNotFoundException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AvitoManagerTest {
    static AvitoManager manager = new AvitoManager();
    static Apartment ap = new Apartment(2, false, false, 2_400_000, 43, true, false, 5, 6, "addr", "description");
    static Apartment ap2 = new Apartment(3, false, false, 4_000_000, 54, false, true, 6, 6, "address", "desc");
    static SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 23, 56, true, false, 1, 7, false, true, 1, 8);

    @Test
    @Order(1)
    void shouldCreate() {
        manager.create(ap);

        List<Apartment> expected = new ArrayList<>();
        expected.add(ap);
        List<Apartment> actual = manager.getAll();

        assertEquals(expected, actual);
    }

    @Test
    @Order(2)
    void shouldGetByIdIfExists() {
        Apartment expected = ap;
        Apartment actual = manager.getById(1);

        assertEquals(expected, actual);
    }

    @Test
    @Order(3)
    void shouldGetByIdIfNotExists() {
        assertThrows(ApartmentNotFoundException.class, () -> manager.getById(999));
    }

    @Test
    @Order(4)
    void shouldUpdateIfNotExists() {
        assertThrows(ApartmentNotFoundException.class, () -> manager.update(ap2));
    }

    @Test
    @Order(5)
    void shouldUpdateIfExists() {
        ap.setPrice(2_600_000);

        Apartment expected = ap;
        Apartment actual = manager.update(ap);

        assertEquals(expected, actual);
    }

    @Test
    @Order(6)
    void shouldGetSize() {
        manager.create(ap2);

        int expected = 2;
        int actual = manager.getCount();

        assertEquals(expected, actual);
    }

    @Test
    @Order(7)
    void shouldRemoveByIdIfExists() {
        manager.removeById(1);
        List<Apartment> expected = new ArrayList<>();
        expected.add(ap2);
        List<Apartment> actual = manager.getAll();

        assertEquals(expected, actual);
    }


    @Test
    @Order(8)
    void shouldRemoveByIdIfNotExists() {
        assertFalse(manager.removeById(999));
    }

    @Test
    @Order(9)
    void shouldSearchIfNotMatch() {
        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(10)
    void shouldSearchIfNotFreeLayout() {
        SearchQuery sq = new SearchQuery(false, true, 1, 4, 1_000_000, 5_000_000, 23, 56, false, true, 2, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(11)
    void shouldSearchIfNotHasBalcony() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 23, 56, true, true, 2, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(12)
    void shouldSearchIfNotHasLoggia() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 23, 56, false, false, 2, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(13)
    void shouldSearchIfNotIsStudio() {
        SearchQuery sq = new SearchQuery(true, false, 1, 4, 1_000_000, 5_000_000, 23, 56, false, true, 2, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(14)
    void shouldSearchIfLessThanMinArea() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 55, 56, false, true, 2, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(15)
    void shouldSearchIfMoreThanMaxArea() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 22, 24, false, true, 2, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(16)
    void shouldSearchIfLessThanMinFloor() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 22, 56, false, true, 7, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(17)
    void shouldSearchIfMoreThanMaxFloor() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 22, 56, false, true, 4, 5, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(18)
    void shouldSearchIfLessThanMinFloorInHouse() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 22, 56, false, true, 1, 7, true, false, 7, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(19)
    void shouldSearchIfMoreThanMaxFloorInHouse() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 22, 56, false, true, 2, 7, false, true, 4, 5);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(20)
    void shouldSearchIfNotFirst() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 22, 56, false, true, 1, 7, true, true, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(21)
    void shouldSearchIfNotLast() {
        ap2.setFloorNumber(1);
        manager.update(ap2);
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 5_000_000, 22, 56, false, true, 1, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);

        ap2.setFloorNumber(6);
        manager.update(ap2);
    }

    @Test
    @Order(22)
    void shouldSearchIfLessThanMinPrice() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 5_000_000, 5_000_000, 22, 56, false, true, 1, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(23)
    void shouldSearchIfMoreThanMaxPrice() {
        SearchQuery sq = new SearchQuery(false, false, 1, 4, 1_000_000, 3_000_000, 22, 56, false, true, 1, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(24)
    void shouldSearchIfEqualsMaxRoomsNumberSearch() {
        SearchQuery sq = new SearchQuery(false, false, 1, 5, 1_000_000, 5_000_000, 22, 56, false, true, 1, 7, true, false, 1, 8);
        manager.search(sq);

        int expected = 99;
        int actual = sq.getRoomsNumberMax();

        assertEquals(expected, actual);
    }

    @Test
    @Order(25)
    void shouldSearchIfLessThanMinRooms() {
        SearchQuery sq = new SearchQuery(false, false, 4, 5, 1_000_000, 5_000_000, 22, 56, false, true, 1, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(26)
    void shouldSearchIfMoreThanMaxRooms() {
        SearchQuery sq = new SearchQuery(false, false, 1, 2, 1_000_000, 5_000_000, 22, 56, false, true, 1, 7, true, false, 1, 8);

        List<Apartment> expected = new ArrayList<>();
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

    @Test
    @Order(27)
    void shouldSearchOverTheLimit() {
        Apartment ap1 = new Apartment(2, false, false, 2_400_000, 43, true, false, 1, 6, "addr", "description");
        Apartment ap7 = new Apartment(2, false, false, 2_400_000, 43, true, false, 5, 6, "addr", "description");
        Apartment ap3 = new Apartment(2, false, false, 2_400_000, 43, true, false, 5, 6, "addr", "description");
        Apartment ap4 = new Apartment(2, false, false, 2_400_000, 43, true, false, 5, 6, "addr", "description");
        Apartment ap5 = new Apartment(2, false, false, 2_400_000, 43, true, false, 5, 6, "addr", "description");
        Apartment ap6 = new Apartment(2, false, false, 2_400_000, 43, true, false, 5, 6, "addr", "description");


        manager.create(ap1);
        manager.create(ap7);
        manager.create(ap3);
        manager.create(ap4);
        manager.create(ap5);
        manager.create(ap6);

        List<Apartment> expected = new ArrayList<>();
        expected.add(ap1);
        expected.add(ap7);
        expected.add(ap3);
        expected.add(ap4);
        expected.add(ap5);
        List<Apartment> actual = manager.search(sq);

        assertEquals(expected, actual);
    }

}
